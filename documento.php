<?php

abstract class Documento
{
    protected $numero;

    abstract public function eValido();

    abstract public function formata();

    public function setNumero($numero)
    {
        $this -> numero = preg_replace('/[^0-9]/', '', $numero); //expressão regular
    }

    public function getNumero()
    {
        return $this -> numero;
    }
}

class CPF extends Documento
{
    public function __construct($numero)
    {
        $this -> setNumero($numero);
    }

    public function eValido()
    {
        $digitoX = 0;
        $somatorioX = 0;
        $cpfX = substr($this-> getNumero(), 0, 9);
        $peso = 10;

        /*
        $somatorioX = $somatorioX + ($cpfX[0] * 10);
        $somatorioX = $somatorioX + ($cpfX[1] * 9);
        $somatorioX = $somatorioX + ($cpfX[2] * 8);
        $somatorioX = $somatorioX + ($cpfX[3] * 7);
        $somatorioX = $somatorioX + ($cpfX[4] * 6);
        $somatorioX = $somatorioX + ($cpfX[5] * 5);
        $somatorioX = $somatorioX + ($cpfX[6] * 4);
        $somatorioX = $somatorioX + ($cpfX[7] * 3);
        $somatorioX = $somatorioX + ($cpfX[8] * 2);
        echo $somatorioX;
        */

        for ($index =0; $index <9; $index ++) {
            $somatorioX += $cpfX[$index] * $peso--;
            //$peso =$peso -1;
            //$peso --;
        }

        echo $somatorioX;

        $modulo11 = $somatorioX % 11;
        echo "</br>" . $modulo11;
    }
    public function formata()
    {
        // formato do CPF: ###.###.###-##
        return substr($this -> numero, 0, 3) . '.' .
        substr($this -> numero, 3, 3) . '.' .
        substr($this -> numero, 6, 3) . '-' .
        substr($this -> numero, 9, 2);
    }
}


class CNPJ extends Documento
{
    public function __construct($numero)
    {
        $this -> setNumero($numero);
    }

    public function eValido()
    {
    }

    public function formata()
    {
        return $this -> numero;
    }
}


class CNH extends Documento
{
    private $categoria;

    public function __construct($numero, $categoria)
    {
        $this -> setNumero($numero);
        $this -> getcategoria = $categoria;
    }

    public function eValido()
    {
    }

    public function formata()
    {
    }

    public function setCategoria($categoria)
    {
        return $this -> categoria;
    }

    public function getCategoria()
    {
        return $this -> categoria;
    }
}

$cpfMatheus = new CPF("501.479.628-11");
$cpfMatheus -> eValido();

/*
$CPF = new CPF('123.456.789-10');
echo $CPF-> formata();

echo'</br>';

$CPF -> setNumero = '123.456.789-10';
echo $CPF -> getNumero();

echo '</br>';
echo '</br>';

$CNPJ = new cnpj('98.765.432/0001-10');
echo $CNPJ-> getNUmero();

echo '</br>';
echo '</br>';

$CNHFulano = new CNH('1255656', 'AB');
echo $CNHFulano-> getNumero(). ' Cat.' . $CNHFulano -> getCategoria();
*/
