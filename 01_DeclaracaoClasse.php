<?php

abstract class Forma
{
    public $tipodeForma = 'Forma Abstrata';

    public function imprimeForma()
    {
        echo $this ->tipodeForma . 'Com Area De: ' . $this -> calculaArea();
    }
        abstract public function calculaArea();
}

class Quadrado extends Forma
{
    public $Lado;

	public function __construct (float $VarLado)

	{
		$this -> tipoDeFoma = 'Quadrado';
		$this -> Lado = $VarLado; 
	}


    public function calculaArea()
    {
        return $this -> Lado * $this -> Lado;
    }
}

"<br>";

class Retangulo extends Forma
{
    public $Base;

	public	$Altura;

	public function __construct ($Base, $Altura)

	{
		$this -> tipoDeFoma = 'Retangulo';
		$this -> Base = $Base; 
		$this -> Altura = $Altura;
	}


    public function calculaArea()
    {
        return $this -> Base * $this -> Altura;
    }
}

$obj = new Quadrado(5);
$obj-> imprimeForma();

$obj1 = new Quadrado(10);
$obj1 -> imprimeForma();




?>


