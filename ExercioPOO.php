<?php

abstract class Conta
{
    private $TipodeConta;
    public function getTipodeConta(){
        return $this -> TipodeConta;
    }
    public function setTipodeConta(string $TipodeConta){
        $this -> TipodeConta = $TipodeConta;
    }

    public $Agencia;
    public $Conta;
    public $Saldo;

    public function imprimeExtrato()
    {
        echo 'Conta: ' . $this->TipodeConta . 'Agencia: ' . $this -> Agencia . 'Numero da Conta: ' . $this -> Conta . 'Saldo: ' . $this -> Saldo;
    }

     public function deposito(float $valor)
     {
        if ($valor >0)
         //$this -> saldo = $this -> saldo + $valor;
         {
            $this -> Saldo  += $valor;
            echo "Depósito efetuado com sucesso <br> ";
         }
         else
         {
             echo "Valor inválido ";
         }


     }

     
    public function Saque(float $valor)
    {
        //$this -> saldo = $this -> saldo - $valor;
        if ($this -> Saldo >= $valor)
       
        {
            $this -> saldo -= $valor;
            echo "Saque realizado com sucesso <br>";
        }
        else 
        {
            echo "Saldo insuficiente <br> ";
        }
    }

    abstract public function CalculaSaldo();
}

class Poupanca extends Conta
{
    public $Reajuste;

    public function __construct(string $Agencia, string $Conta, float $Reajuste)
    {
        $this -> Agencia = $Agencia;
        $this -> Conta = $Conta;
        $this -> Reajuste = $Reajuste;
        $this -> setTipodeConta ('Poupança');
    }

    public function CalculaSaldo()
    {
        return $this -> Saldo + ($this -> Saldo * $this -> Reajuste / 100);
    }
}

class Especial extends Conta
{
    public $SaldoEspecial;

    public function __construct(string $Agencia, string $Conta, float $SaldoEspecial)
    {
        $this -> Agencia = $Agencia;
        $this -> Conta = $Conta;
        $this -> SaldoEspecial = $SaldoEspecial;
        $this -> setTipodeConta ('Especial');
    }

    public function CalculaSaldo()
    {
        return $this -> Saldo + $this ->SaldoEspecial;
    }
}


$ctaPoupanca = new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoupanca -> saldo =-1500;
// Não pode acessar atributo protegido
$ctaPoupanca -> deposito(1500);
$ctaPoupanca -> saque(3000);
$ctaPoupanca -> imprimeExtrato();

echo "<br>";

$ctaEspecial = new Especial('0055-2', '75588-42', 2300);
$ctaEspecial -> deposito(1500);
$ctaEspecial -> imprimeExtrato();
